// Задание_1

let milkmanPhrase = "Молоко - это полезно"

print(milkmanPhrase)

// Задание_2-3

var milkPrice: Double = 3

milkPrice = 4.20

print ("Стоимость 1 бутылки молока - " + String(milkPrice)) // поработала с конкатенацией строк и вывести информацию о стоимости молока

// Задание_4*

let milkBottleCount: Int? = 20
var profit: Double = 0.0

/*
 Использовала безопасное развертывание milkBottleCount с помощью механизма optional binding с созданием временной переменной actualMilkBottleCount и обработкой случая, когда milkBottleCount == nil
 
 Опционал подразумевает возможность отсутствия какого-либо у него значения (nil).
 Безопасное развертывание необходимо, чтобы предотвратить ошибку неожиданного обнаружения значения nil.
 В случае если мы уверены, что опционал всегда будет иметь непустое значение, можно использовать например неявное извлечение (implicity unwrapping).
 */

if let actualMilkBottleCount = milkBottleCount {
    profit = milkPrice * Double(actualMilkBottleCount)
} else {
    print("Упс! Кузов пустой, вернись за бутылками") // блоке else может отсутствовать, но решила добавить объяснение причины нулевой выручки
}

print("Твоя выручка - " + String(profit))

// Задание_5

var employeesList: [String] = [] //по заданию нужно было сначала объявить пустой массив, хотя кажется, что можно было сразу присвоить ему значение
employeesList = ["Иван", "Марфа", "Андрей", "Петр", "Геннадий"]

print("Работяги: " + employeesList.joined(separator: ", ")) // решила красиво вывести список всех работников

// Задание_6

let isEveryoneWorkHard: Bool = false
let workingHours: Double = 39.5 // Double - так как в задании явно не указан тип, а работник может отработать дробное количество часов
if workingHours >= 40 {
    print(!isEveryoneWorkHard)
} else {
    print(isEveryoneWorkHard)
}
